////  ViewController.swift
//  TableViewPlaceholderAndStatesFromHGPlaceholders
//
//  Created on 07/06/2020.
//  Copyright © 2020 kostowski. All rights reserved.
//

import HGPlaceholders
import UIKit

class ViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {

    private let tableView = TableView()
    
//    var data = ["one" , "two" , "three"]
    var data: [String] = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        view.addSubview(tableView)
        
        tableView.delegate      = self
        tableView.dataSource    = self
        
        tableView.frame = view.bounds
        
        if data.isEmpty {
            //tableView.isHidden = true
            tableView.showErrorPlaceholder()
        }
        
        tableView.backgroundColor = .link
        tableView.showLoadingPlaceholder()
        
        DispatchQueue.main.asyncAfter(deadline: .now()+2) {
            
//            self.data.append("XYZ")
//            self.data.append("XYZ")
//            self.data.append("XYZ")

            self.tableView.showDefault()
            self.tableView.reloadData()
//
//            self.tableView.showErrorPlaceholder()
        }
        
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        cell.textLabel?.text = data[indexPath.row]
        return cell
    }

}

